import { ServerIdEnum } from "../../common";
import { Scene } from "../base/Scene";

export class Scene2 extends Scene {
  id = ServerIdEnum.Scene2;
}

new Scene2().init();
