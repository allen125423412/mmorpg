import { ServerIdEnum } from "../../common";
import { Scene } from "../base/Scene";

export class Scene1 extends Scene {
  id = ServerIdEnum.Scene1;
}

new Scene1().init();
