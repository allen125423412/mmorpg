import express from "express";
import { v4 as uuid } from "uuid";
import bodyParser from "body-parser";
import cors from "cors";
import { verifyAndParse } from "./middleware";
import { CodeEnum } from "./enum";
import { createRes, md5 } from "./utils";
import { ServerPort, createDBConnection, Logger } from "../../common";
import dayjs from "dayjs";

export const cache = new Map<string, string>();

export const httpStart = async () => {
  const connection = createDBConnection();

  const logger = new Logger();

  const app = express();
  app.use(cors());
  app.use(bodyParser.json());

  app.post("/register", verifyAndParse, function (req, res) {
    const { account, password } = req.body;

    connection.query(
      `insert into user (account, password, created_time) VALUES (?, ?, ?)`,
      [account, md5(password), dayjs().format("YYYY-MM-DD HH:mm:ss")],
      (err) => {
        if (err) {
          if (err.errno === 1062) {
            res.json(createRes(CodeEnum.AccountExist));
            return;
          }
          res.json(createRes(CodeEnum.SqlError));
          return;
        }

        res.json(createRes(CodeEnum.RegistrySuccess));
      }
    );
  });

  app.post("/login", verifyAndParse, function (req, res) {
    const { account, password } = req.body;

    connection.query(`select password from user where account = ?`, [account], (err, result) => {
      if (err) {
        res.json(createRes(CodeEnum.SqlError));
        return;
      }

      const user = result[0];
      if (!user || md5(password) !== user.password) {
        res.json(createRes(CodeEnum.UsernameOrPasswordError));
        return;
      }

      const token = uuid();
      cache.set(token, account);

      res.json(
        createRes(CodeEnum.LoginSuccess, {
          token,
        })
      );
    });
  });

  app.listen(ServerPort.AuthHttp, () => {
    logger.info("Auth HTTP服务启动");
  });
};
